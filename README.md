# WFacts

Language used: Swift 5 Xcode ver: 11.2.1
Design Patterns: MVVM 


Features: 
1. As mentioned in problem pdf, Used NSLayout for creating constraints (without storyboard)
2. Image caching
3. Unit testing
4. Reachability test
5. Handled Error scenarios
6. Swift Codable to parse JSON
7. Alamofire for network calls
8. Pull-To-Refresh
