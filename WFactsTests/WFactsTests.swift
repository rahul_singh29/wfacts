//
//  WFactsTests.swift
//  WFactsTests
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import XCTest
@testable import WFacts

class WFactsTests: XCTestCase {
    
    var viewController: FactsCollectionViewController!


    override func setUp() {
        self.viewController = FactsCollectionViewController()
        self.viewController.factsViewModels = [FactsViewModel(facts: Facts(title: "Title", description: "This is description", imageStrUrl: "url"))]
        self.viewController.loadView()
        self.viewController.viewDidLoad()
    }

    override func tearDown() {
        self.viewController = nil
    }
    
    func testHasATableView() {
        XCTAssertNotNil(viewController.collectionView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(viewController.collectionView.delegate)
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(viewController.collectionView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(viewController.conforms(to: UICollectionViewDataSource.self))
        XCTAssertTrue(viewController.responds(to: #selector(viewController.collectionView(_:numberOfItemsInSection:))))
        XCTAssertTrue(viewController.responds(to: #selector(viewController.collectionView(_:cellForItemAt:))))
    }

    func testTableViewCellHasReuseIdentifier() {
        let cell = viewController.collectionView(viewController.collectionView, cellForItemAt: IndexPath(row: 0, section: 0)) as? FactsCollectionViewCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = "factsCell"
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier)
    }

    func testFactsViewModal() {
        let facts = Facts(title: "title", description: "description", imageStrUrl: "")
        let factsViewModel = FactsViewModel(facts: facts)
        XCTAssertEqual(facts.title, factsViewModel.title)
        XCTAssertEqual(facts.description, factsViewModel.description)
    }

    func testFactsViewModalWithNilData() {
        let facts = Facts(title: nil, description: nil, imageStrUrl: nil)
        let factsViewModel = FactsViewModel(facts: facts)
        XCTAssertEqual(facts.title, factsViewModel.title)
        XCTAssertEqual(facts.description, factsViewModel.description)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
