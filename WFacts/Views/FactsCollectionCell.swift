//
//  FactTableViewCell.swift
//  WFacts
//
//  Created by Rahul Singh on 03/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class FactsCollectionViewCell: UICollectionViewCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let kImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let seperator: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var factsViewModel: FactsViewModel! {
        didSet{
            self.titleLabel.text = factsViewModel.title
            self.descriptionLabel.text = factsViewModel.description
            if let url = factsViewModel.imageUrl {
                self.kImageView.downloadImage(url)
            }
            self.layoutIfNeeded()
            self.setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
    
    func addViews(){
        self.backgroundColor = .white
        self.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(kImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(seperator)

        kImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        kImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        kImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true

        titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10).isActive = true
        titleLabel.topAnchor.constraint(equalTo: kImageView.bottomAnchor, constant: 10).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10).isActive = true

        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.lastBaselineAnchor, constant: 15).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: titleLabel.rightAnchor).isActive = true
        descriptionLabel.bottomAnchor.constraint(equalTo: seperator.topAnchor, constant: -10).isActive = true

        seperator.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        seperator.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        seperator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        seperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
}

var imageCache = Dictionary<String,UIImage>()
extension UIImageView {
    
    func downloadImage(_ imageurl: URL) {
        image = nil
        if let kImage = imageCache[imageurl.absoluteString] {
            self.image = kImage
        } else {
            Alamofire.request(imageurl, method: HTTPMethod.get).responseImage { response in
                if response.result.error != nil {
                    DispatchQueue.main.async {
                        imageCache[imageurl.absoluteString] = nil
                        self.image = nil
                    }
                } else if let image = response.result.value {
                    DispatchQueue.main.async {
                        imageCache[imageurl.absoluteString] = image
                        self.image = image
                    }
                }
            }
        }
    }
}
