//
//  Constants.swift
//  WFacts
//
//  Created by Rahul Singh on 03/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class Alert {
    static let shared = Alert()
    
    func show(view: UIViewController, withTitle: String = "", message:String) {
        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        DispatchQueue.main.async {
            view.present(alert, animated: true, completion: nil)
        }
    }
}

struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet: Bool {
        return self.sharedInstance.isReachable
    }
}
