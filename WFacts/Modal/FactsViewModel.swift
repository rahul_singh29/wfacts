//
//  FactsViewModal.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation


struct FactsViewModel {
    
    let title: String?
    let description: String?
    let imageUrl: URL?
    
    init(facts: Facts) {
        self.title = facts.title
        self.description = facts.description
        if let urlStr = facts.imageStrUrl, let kUrl = URL(string: urlStr) {
            self.imageUrl = kUrl
        } else {
            self.imageUrl = nil
        }
    }
    
}
