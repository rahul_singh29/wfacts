//
//  Facts.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation

typealias Facts = Fact.Rows

struct Fact: Codable {
    
    var title: String?
    var rows : [Rows]?
    
    struct Rows: Codable {
        var title: String?
        var description: String?
        var imageStrUrl: String?
    }
}

extension Fact.Rows {
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case imageStrUrl = "imageHref"
    }
}
