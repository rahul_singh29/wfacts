//
//  FactsRequest.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation

extension ApiClient {
    
    func getfacts(handler: RegularRequestCompletion<Fact>) {
        request(path: "/facts.json", handler: RequestCompletion.regular(completion: handler))
    }
    
}
