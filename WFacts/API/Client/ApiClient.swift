//
//  ApiClient.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation
import Alamofire
import UIKit


class ApiClient: SessionManager {
    static let shared = ApiClient(configuration: ApiClient.urlSessionConfiguration(), serverTrustPolicyManager: nil)
    static func urlSessionConfiguration() -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.httpCookieStorage = nil
        configuration.httpCookieAcceptPolicy = .never
        configuration.httpShouldSetCookies = false
        return configuration
    }
    
    static var apiDomain = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl"
    
    
    func request<T: Codable>(method: Alamofire.HTTPMethod = .get, path: String, encoding: ParameterEncoding = JSONEncoding(), handler: RequestCompletion<T>) {
        
        // Send request
        let endpointURI = ApiClient.apiDomain + path
        let request = super.request(endpointURI, method: method, encoding: encoding)

        request.responseData(completionHandler: { (response) -> Void in
            switch response.result {
            case .success(_):
                let decoder = JSONDecoder()
                do {
                    if let datastring = NSString(data: response.data!, encoding: String.Encoding.isoLatin1.rawValue) {
                        let data = datastring.data(using: String.Encoding.utf8.rawValue)
                        let responseObject = try decoder.decode(T.self, from: data!)
                        switch handler {
                        case .regular(let completion): completion.success(responseObject)
                        }
                    } else {
                        return handler.failure!(NSError.errorWithDefaultMessage())
                    }
                } catch let error {
                     return handler.failure!(NSError.errorWithMessage(error.localizedDescription))
                }
            case .failure:
                handler.failure?(NSError.errorWithMessage(""))
            }
        })
    }
}
