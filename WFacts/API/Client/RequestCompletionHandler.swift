//
//  RequestCompletionHandler.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation

struct RegularRequestCompletion<Response> {
    let success: (Response) -> ()
    let failure: ((Error) -> ())?
    
    init(success: @escaping (Response) -> (), failure: ((Error) -> ())? = nil) {
        self.success = success
        self.failure = failure
    }
}

enum RequestCompletion<Response> {
    case regular(completion: RegularRequestCompletion<Response>)
    
    var failure: ((Error) -> ())? {
        switch self {
        case .regular(let completion): return completion.failure
        }
    }
}
