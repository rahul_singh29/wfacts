//
//  FactsViewController.swift
//  WFacts
//
//  Created by Rahul Singh on 02/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import UIKit

enum CellDisplayStyle {
    case table
    case grid
}


class FactsCollectionViewController: UIViewController {

    var factsViewModels = [FactsViewModel]()
    var collectionView: UICollectionView!
    var flowLayout: UICollectionViewFlowLayout!
    private let cellReuseIdentifier = "factsCell"
    var krefreshControl = UIRefreshControl()
    private var layoutOption: CellDisplayStyle = .table {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewSetup()
        self.getFacts()
    }
    
    func getFacts() {
        if Connectivity.isConnectedToInternet == false {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                Alert.shared.show(view: self, withTitle: "No Internet Connection", message: "Make sure your device is connected to the internet.")
                self.krefreshControl.endRefreshing()
            }
            return
        }
        LoadingOverlay.shared.showOverlay(view: self.view)
        ApiClient.shared.getfacts(handler: RegularRequestCompletion<Fact>(success: { (response) in
            if let title = response.title {
                self.title = title
            }
            LoadingOverlay.shared.hide()
            if let facts = response.rows {
                self.factsViewModels = facts.map({return FactsViewModel(facts: $0)}).filter { ($0.description != nil && $0.title != nil && $0.imageUrl != nil) }
                self.layoutOption = self.traitCollection.horizontalSizeClass == .regular ? .grid : .table
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    self.collectionView.layoutIfNeeded()
                    self.collectionView.collectionViewLayout.invalidateLayout()
                }
            } else {
                Alert.shared.show(view: self, message: "Something went wrong")
            }
        }, failure: { (error) in
            Alert.shared.show(view: self, message: "Something went wrong")
            LoadingOverlay.shared.hide()
        }))
    }

    @objc func refresh() {
        self.getFacts()
        self.krefreshControl.endRefreshing()
    }
    

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.layoutOption = self.traitCollection.horizontalSizeClass == .regular ? .grid : .table
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        self.layoutOption = self.traitCollection.horizontalSizeClass == .regular ? .grid : .table
    }
}

// MARK: ----------------- COLLECTION VIEW -------------------

extension FactsCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.factsViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath) as! FactsCollectionViewCell
        cell.factsViewModel = self.factsViewModels[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let kheight: CGFloat = 200
        switch layoutOption {
        case .table:
            if traitCollection.horizontalSizeClass == .regular {
                return CGSize(width: (view.bounds.size.width/2)-50, height: kheight)
            } else if traitCollection.horizontalSizeClass == .compact {
                return CGSize(width: view.bounds.size.width-50, height: kheight)
            }
        case .grid:
            let minItemWidth: CGFloat = (view.bounds.size.width/2)-50
            return CGSize(width: minItemWidth, height: kheight)
        }
        return CGSize(width: collectionView.bounds.size.width, height: kheight)
    }
    
    func collectionViewSetup() {
        flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        self.collectionView.register(FactsCollectionViewCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
        self.addConstraintsToCollectionView()
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.krefreshControl.tintColor = UIColor.black
        self.krefreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        
    }
    
    func addConstraintsToCollectionView() {
        self.view.addSubview(self.collectionView)
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.collectionView.backgroundColor = UIColor.white
        self.collectionView.alwaysBounceVertical = true
        let guide = self.view.safeAreaLayoutGuide
        self.collectionView.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
        self.collectionView.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        self.collectionView.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        self.collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.collectionView.addSubview(krefreshControl)
    }
}
