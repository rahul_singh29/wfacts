//
//  Loader.swift
//  WFacts
//
//  Created by Rahul Singh on 03/12/19.
//  Copyright © 2019 RahulSingh. All rights reserved.
//

import Foundation
import UIKit

public class LoadingOverlay {

    var overlayView : UIView!
    var activityIndicator : UIActivityIndicatorView!

    static var shared: LoadingOverlay = LoadingOverlay()

    init(){
        self.overlayView = UIView()
        self.activityIndicator = UIActivityIndicatorView()

        overlayView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        overlayView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        overlayView.layer.zPosition = 1

        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        activityIndicator.style = .whiteLarge
        overlayView.addSubview(activityIndicator)
    }

    public func showOverlay(view: UIView) {
        overlayView.center = view.center
        view.addSubview(overlayView)
        activityIndicator.startAnimating()
    }

    public func hide() {
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
}

class LoadingView: UIView {
    static var shared: LoadingView = LoadingView()
   
    private var activityIndicatorView: UIActivityIndicatorView {
        
        let view = UIActivityIndicatorView(style: .white)
        view.startAnimating()
        
        view.center = self.center
        return view
    }
    
    private var isPresenting: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .black
        self.alpha = 0.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func present() {
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(self)
        fadeIn()
    }
    
    func dismiss() {
        guard isPresenting == true else { return }
        fadeOut()
    }
    
    private func fadeIn() {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            self.alpha = 0.5
        }) { (bool: Bool) in
            self.addSubview(self.activityIndicatorView)
            self.isPresenting = true
        }
    }
    
    private func fadeOut() {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            self.alpha = 0.0
        }) { (bool: Bool) in
            self.removeFromSuperview()
            self.isPresenting = false
        }
    }
}
